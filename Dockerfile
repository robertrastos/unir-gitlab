FROM python:3.6-slim

ENV PYTHONPATH=/opt/calc
ENV FLASK_APP=app/api.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000
ENV FLASK_ENV=development

WORKDIR /opt/calc
RUN mkdir -p /opt/calc

COPY .coveragerc .pylintrc pyproject.toml pytest.ini requires ./
COPY app ./app
COPY test ./test
RUN pip install -r requires
