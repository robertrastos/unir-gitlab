set +e

echo Checking registry for image $CONTAINER_COMMIT_API_IMAGE
docker pull $CONTAINER_COMMIT_API_IMAGE
if [[ $? -eq 0 ]]
then 
    echo Image $CONTAINER_COMMIT_API_IMAGE exists, skip building.
else
    echo Image $CONTAINER_COMMIT_API_IMAGE does not exist, building...
    docker build --pull -t $CONTAINER_COMMIT_API_IMAGE .
    export BUILD_RESULT=$?
    if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
    docker push $CONTAINER_COMMIT_API_IMAGE
    export BUILD_RESULT=$?
    if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
fi

echo Checkig registry for image $CONTAINER_COMMIT_WEB_IMAGE
docker pull $CONTAINER_COMMIT_WEB_IMAGE
if [[ $? -eq 0 ]]
then 
    echo Image $CONTAINER_COMMIT_WEB_IMAGE exists, skip building.
else
    echo Image $CONTAINER_COMMIT_WEB_IMAGE does not exist, building...
    docker build --pull -t $CONTAINER_COMMIT_WEB_IMAGE ./web
    export BUILD_RESULT=$?
    if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
    docker push $CONTAINER_COMMIT_WEB_IMAGE
    export BUILD_RESULT=$?
    if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
fi
